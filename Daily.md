# 王昭昭

## 2021.8.11
1. 文章阅读

- [HTML 背诵（1）—— ](https://juejin.cn/post/6950826293923414047)
- [手写JS！各种姿势解数组去重和数组扁平化](https://juejin.cn/post/6950307682737717261)

2. 源码阅读



3. leecode 刷题

- [字符串转换整数 (atoi)](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnoilh/)
- [实现 strStr()](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnr003/)

4. 项目进度

- [] 班级管理页面布局
- [] 添加班级 删除班级
- [] 教室管理页面布局
- [] 添加教室号 删除教室
- [] 学生管理页面布局
